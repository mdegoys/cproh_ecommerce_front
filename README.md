# Description

  This repository is part of a school projet to create a mini e-commerce app. This is the front-end part.

# Getting Started

  To use it, clone/fork and install the dependencies :

  `npm install`

  Then to start it, use the following command :

  `npm start`

# Data Structure

  All files are in the src folder, with :
  - App.js containing all the main state and methods
  - components subfolders having small components for additional displays (cart details, forms and information messages)
