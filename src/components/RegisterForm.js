import React, { Component } from 'react';

class RegisterForm extends Component {
  state = {
    username: '',
    password: ''
  }

  handleChange = (e) => {
    this.setState({ [e.target.id]: e.target.value });
  }

  render() {
    return(
      <div className="form">
        <input className="input" type="text" id="username" value={this.state.username} onChange={this.handleChange} placeholder="Enter username"/>
        <input className="input" type="password" id="password" value={this.state.password} onChange={this.handleChange} placeholder="Enter password"/>
        <button className="btn-action" type="submit" value="Register" onClick={() => this.props.postRegister(this.state.username, this.state.password)}>Register</button>
      </div>
    );
  }
}

export default RegisterForm;
