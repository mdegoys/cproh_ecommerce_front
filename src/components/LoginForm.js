import React, { Component } from 'react';

class LoginForm extends Component {
  state = {
    username: '',
    password: ''
  }

  handleChange = (e) => {
    this.setState({ [e.target.id]: e.target.value });
  }

  render() {
    return(
      <div className="form">
        <input className="input" type="text" id="username" value={this.state.username} onChange={this.handleChange} placeholder="Enter your username"/>
        <input className="input" type="password" id="password" value={this.state.password} onChange={this.handleChange} placeholder="Enter your password"/>
        <button className="btn-action" type="submit" value="Log in" onClick={() => this.props.postLogin(this.state.username, this.state.password)}>Log in</button>
      </div>
    );
  }
}

export default LoginForm;
