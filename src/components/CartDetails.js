import React from 'react';

const CartDetails = (props) => {
  return(
    <div className="cart-details">{props.cart.map(x => {
      var indexProduct = props.products.findIndex(z => z.name === x['product_name']);
      var productPrice = props.products[indexProduct]['price']
      var productImage = props.products[indexProduct]['image']
        return (
          <div className="detailsAll">
            <div className="image-productCart">
              <img className="img-cart" src={productImage}></img>
            </div>
            <div className="details-productCart">
              <p>{x.product_name}</p> 
              <p className="price">{productPrice} €</p> 
              <p>Qty: <input type="number" id={x.product_name} value={x.qty} min="0" max="99" onChange={props.setQty} /></p>
            </div>
            <button className="btn-delete" id={x.product_name} onClick={props.removeFromCart}>Remove</button>
          </div>
        )
    })}</div>
  );
}

export default CartDetails;
