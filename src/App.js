import React, { Component } from 'react';
import CartDetails from './components/CartDetails';
import LoginForm from './components/LoginForm';
import RegisterForm from './components/RegisterForm';
import ResultMessage from './components/ResultMessage';
import './App.css';

class App extends Component {
  state = {
    products : [],
    cart: [],
    displayCart: false,
    displayForm: '',
    username: '',
    resultType: '',
    resultMessage: ''
  }

  // là, je fais appelle à mon back
  componentDidMount() {
    fetch('http://localhost:1234/products')
    .then(res => res.json())
    .then(products => this.setState( { products } ))
    .catch(err => console.log(err))

    this.checkLocalStorage();
  }

  checkLocalStorage = () => {
    var username = localStorage.getItem("username");
    if (username) {
      this.setState({ username });
    }
  }

  // et là, je gère le click sur un bouton d'ajout au panier
  addToCart = (e) => {
    var newCart = [...this.state.cart]; // on créé une copie du tableau avec le spread operator ...
    // je regarde si mon tablau this.state.cart contient ou pas mon produit:

    if (this.state.cart.some(x => x.product_name === e.target.value)) {
    // Si oui : j'incrémente sa quantité après avoir trouvé son index
      var indexProduct = this.state.cart.findIndex(x => x.product_name === e.target.value);
      newCart[indexProduct]['qty']++;

    } else {
    // Sinon : je le créé avec une quantité de 1
      newCart.push({ "product_name": e.target.value, "qty": 1 });
    }

    // Enfin, je remplace this.state.cart avec mon newCart
    this.setState({ cart: newCart }, () => { this.postCart() });
  }

  removeFromCart = (e) => {
    var newCart = [...this.state.cart];
    var indexProduct = newCart.findIndex(x => x.product_name === e.target.id);
    newCart.splice(indexProduct,1);
    this.setState({ cart: newCart }, () => { this.postCart(); });
  }

  viewCart = () => {
    this.setState({ displayCart: !this.state.displayCart })
  }

  viewForm = (form) => {
    this.setState({ displayForm: form })
  }

  setQty = (e) => {
    var newCart = [...this.state.cart];
    var indexProduct = this.state.cart.findIndex(x => x.product_name === e.target.id);
    newCart[indexProduct]['qty'] = e.target.value;
    this.setState({ cart: newCart }, () => { this.postCart(); });
  }

  displayLogin = () => {
    if (this.state.username === '') {
      return (
        <div><span onClick={() => this.viewForm('')}>You are not logged in.</span> <button onClick={() => this.viewForm('login')}>Log in</button><button onClick={() => this.viewForm('register')}>Register</button></div>
      )
    } else {
      return (
        <span>You are logged in as {this.state.username }. <button className="btn-logout" onClick={this.logOut}>Log out</button>.</span>
      )
    }
  }

  displayTotalPrice = () => {
    return(
    <>
    Total price :
    {/*Deuxième reduce : pour sommer de même les quantités, mais cette fois-ci multipliées par le prix de l'article correspondant*/}
    {' '+this.state.cart.reduce((x,y) => {
      var indexProduct = this.state.products.findIndex(z => z.name === y['product_name']);
      var productPrice = this.state.products[indexProduct]['price']
      return Number(x) + Number(y['qty'])*productPrice;
    },0)} € <button className="btn-delete" onClick={this.clearCart}>Clear cart</button>
    </>
    )}

  postLogin = (username, password) => {
    var body = JSON.stringify({ username, password })
    const headers = {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
		}
    fetch('http://localhost:1234/users/login', {
      method: 'POST',
      headers,
      body
    })
    .then(res => res.json())
    .then(data => {
      this.setState({ resultType: data.resultType, resultMessage: data.resultMessage });

      if (data.resultType === 'success') {
        localStorage.setItem("token",data.token)
        localStorage.setItem("username",username)
        this.setState({ username, displayForm: '' }, () => {
          this.getCart(); });
      }
    })
    .catch(err => console.warn(err))
  }

  postRegister = (username, password) => {
    const headers = {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
		}
    var body = JSON.stringify({ username, password })
    fetch('http://localhost:1234/users/register', {
      method: 'POST',
      headers,
      body
    })
    .then(res => res.json())
    .then(data => {
      this.setState({ resultType: data.resultType, resultMessage: data.resultMessage });
      if (data.resultType === 'success') {
        this.setState({ displayForm: '' });
      }
    })
    .catch(err => console.warn(err))
  }

  getCart = () => {
    var headers = { 'token': localStorage.getItem('token') }
    fetch(`http://localhost:1234/carts/${this.state.username}`, { headers })
    .then(res => res.json())
    .then(data => {
      if (data.resultType === 'success') {
        var storedCart = JSON.parse(data.cart);

        // Si le panier existant est vide, on charge le panier sauvegardé
        if (this.state.cart.length === 0) {
          this.setState({ cart: storedCart });

        // Sinon on le sauvegarde
        } else {
          this.postCart();
        }
      }
    })
    .catch(err => console.warn(err))
  }

  postCart = () => {
    if (this.state.username) {
      var headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'token': localStorage.getItem('token')
      }
      var body = JSON.stringify({ cart: this.state.cart })
      fetch(`http://localhost:1234/carts/${this.state.username}`, { method: 'POST', headers, body })
      .then(res => res.json())
      .then(data => {
      })
      .catch(err => console.warn(err))
    }
  }

  clearCart = () => {
    this.setState({ cart: [] }, () => {
      this.postCart();
      this.setState({ displayCart: false });
    });
  }

  clearResult = () => {
    this.setState({ resultType: '', resultMessage: '' });
  }

  logOut = () => {
    this.clearCart();
    this.setState({ username: '' });
    localStorage.removeItem("token");
    localStorage.removeItem("username");
  }

  render() {
    return (
      <>
        <div>{this.state.resultMessage && <ResultMessage type={this.state.resultType} message={this.state.resultMessage} clearResult={this.clearResult} />}</div>
        <div className="App">
          <div className="Header">
            <div id="cart">
            {/* On utilise reduce pour transformer un tableau (le réduire) en un total :
            Premier reduce : pour sommer les quantités de chaque produit */ }
            <span onClick={this.viewCart}><span className="span-cart">Cart:</span><br></br> <span className="span-products">Product{this.state.cart.length > 1 ? 's ' : ' '} :{this.state.cart.reduce((x,y) => Number(x) + Number(y['qty']),0)}</span><br></br>
            {this.state.cart.length > 0 && this.displayTotalPrice()}
            </span>
            {/* On affiche CartDetails que si displayCart est true */}
            {this.state.displayCart && <CartDetails cart={this.state.cart} products={this.state.products} setQty={this.setQty} removeFromCart={this.removeFromCart} />}
          </div>
          <div id="user">
            { this.displayLogin() }
            {this.state.displayForm === 'login' && <LoginForm postLogin={this.postLogin} />}
            {this.state.displayForm === 'register' && <RegisterForm postRegister={this.postRegister} />}
          </div>
          </div>
          <div id="products">
            {this.state.products.map((x,i) => <div key={i} className="product">
            <img src={x.image}></img>
            <p>{x.name}</p>
            <p className="price">{x.price} €</p>
            <button className="btn-add" value={x.name} onClick={this.addToCart}>Add to cart</button>
            </div>)}
          </div>
        </div>
      </>
    );
  }
}

export default App;
